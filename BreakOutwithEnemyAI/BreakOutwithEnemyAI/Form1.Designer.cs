﻿namespace BreakOutwithEnemyAI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.Player = new System.Windows.Forms.Button();
            this.Enemy = new System.Windows.Forms.Button();
            this.Ball = new System.Windows.Forms.RadioButton();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.GoalPostEnemy = new System.Windows.Forms.Panel();
            this.GoalPostPlayer = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.GoalPostEnemy.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 20;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Player
            // 
            this.Player.Location = new System.Drawing.Point(169, 461);
            this.Player.Name = "Player";
            this.Player.Size = new System.Drawing.Size(141, 23);
            this.Player.TabIndex = 0;
            this.Player.Tag = "Player";
            this.Player.Text = "Player";
            this.Player.UseVisualStyleBackColor = true;
            this.Player.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keyisdown);
            this.Player.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyisup);
            // 
            // Enemy
            // 
            this.Enemy.Location = new System.Drawing.Point(169, 12);
            this.Enemy.Name = "Enemy";
            this.Enemy.Size = new System.Drawing.Size(141, 23);
            this.Enemy.TabIndex = 1;
            this.Enemy.Tag = "Enemy";
            this.Enemy.Text = "Enemy";
            this.Enemy.UseVisualStyleBackColor = true;
            this.Enemy.Click += new System.EventHandler(this.Enemy_Click);
            // 
            // Ball
            // 
            this.Ball.AutoSize = true;
            this.Ball.BackColor = System.Drawing.Color.Transparent;
            this.Ball.Location = new System.Drawing.Point(235, 264);
            this.Ball.Name = "Ball";
            this.Ball.Size = new System.Drawing.Size(14, 13);
            this.Ball.TabIndex = 2;
            this.Ball.TabStop = true;
            this.Ball.Tag = "Ball";
            this.Ball.UseVisualStyleBackColor = false;
            this.Ball.CheckedChanged += new System.EventHandler(this.Ball_CheckedChanged);
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 1;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // GoalPostEnemy
            // 
            this.GoalPostEnemy.Controls.Add(this.label2);
            this.GoalPostEnemy.Location = new System.Drawing.Point(2, 3);
            this.GoalPostEnemy.Name = "GoalPostEnemy";
            this.GoalPostEnemy.Size = new System.Drawing.Size(476, 14);
            this.GoalPostEnemy.TabIndex = 3;
            this.GoalPostEnemy.Tag = "GoalPostEnemy";
            this.GoalPostEnemy.Paint += new System.Windows.Forms.PaintEventHandler(this.GoalPostEnemy_Paint);
            // 
            // GoalPostPlayer
            // 
            this.GoalPostPlayer.Location = new System.Drawing.Point(2, 480);
            this.GoalPostPlayer.Name = "GoalPostPlayer";
            this.GoalPostPlayer.Size = new System.Drawing.Size(476, 14);
            this.GoalPostPlayer.TabIndex = 4;
            this.GoalPostPlayer.Tag = "GoalPostPlayer";
            this.GoalPostPlayer.Paint += new System.Windows.Forms.PaintEventHandler(this.GoalPostPlayer_Paint);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Red;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(53, 195);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(415, 32);
            this.label1.TabIndex = 5;
            this.label1.Text = "label1";
            this.label1.Visible = false;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 32);
            this.label2.TabIndex = 6;
            this.label2.Text = "label2";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label3.Location = new System.Drawing.Point(-1, 445);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 32);
            this.label3.TabIndex = 7;
            this.label3.Text = "label3";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(480, 496);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.GoalPostPlayer);
            this.Controls.Add(this.GoalPostEnemy);
            this.Controls.Add(this.Ball);
            this.Controls.Add(this.Enemy);
            this.Controls.Add(this.Player);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.GoalPostEnemy.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button Player;
        private System.Windows.Forms.Button Enemy;
        private System.Windows.Forms.RadioButton Ball;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Panel GoalPostEnemy;
        private System.Windows.Forms.Panel GoalPostPlayer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}

