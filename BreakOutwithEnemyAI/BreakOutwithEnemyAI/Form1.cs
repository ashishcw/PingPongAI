﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Threading;

namespace BreakOutwithEnemyAI
{
    public partial class Form1 : Form
    {
        static Form1 _frmObj;
        public static Form1 frmObj
        {
            get { return _frmObj; }
            set { _frmObj = value; }
        }

        //public Form1 mainform = new Form1();
        //public Form2 titleform = new Form2();
        Form mainformm = new Form2();

        public bool goright, goleft, cpuplayerturn, correcthitCPU, easy, medium, hard; //Flags to check ball movment and direction        
        private int Speed = 20; //Assigned Velocity to Player
        private int EnemySpeed = 50; //Assigned Velocity to CPU
        private int ballX = 08;
        private int ballY = 08; //x and y holding position
        private int targetscore, temprnd;
        private int PlayerScore = 0; //Player Score for current session
        private int EnemyScore = 0; //Enemy Score for current session
        private Random Rnd = new Random();
        private System.Windows.Threading.DispatcherTimer TimeSpent = new System.Windows.Threading.DispatcherTimer();
        

        public Form1()
        {
            InitializeComponent();
            targetscore = Decimal.ToInt32(Form2.frmObj2.temptargetscore);
            timer1.Enabled = false;
        }

        private bool ballfrozen = false;
        private void keyisdown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Space)
            {
                timer1.Enabled = !timer1.Enabled;
                if (label1.Visible)
                {
                    label1.Visible = false;
                }
            }
            if (timer1.Enabled)
            {
                if (e.KeyCode == Keys.A || e.KeyCode == Keys.Left && Player.Left > 0)
                {
                    goleft = true;
                }

                //if (e.KeyCode == Keys.Right && Player.Right > 0)
                if (e.KeyCode == Keys.Right || e.KeyCode == Keys.D && Player.Left + Player.Width < 500)
                {
                    goright = true;
                }

                //if (e.KeyCode == Keys.Space && ballfrozen == false)
                //{
                //    Ball.Location = Player.Location;
                //    ballfrozen = true;
                //}
            }
        }

        enum LevelSelect
        {
            easy, medium, hard
        };

        private void keyisup(object sender, KeyEventArgs e)
        {
            if (timer1.Enabled)
            {
                if (e.KeyCode == Keys.Left || e.KeyCode == Keys.A)
                {
                    if (goleft == true)
                    {
                        goleft = false;
                    }
                }
                if (e.KeyCode == Keys.Right || e.KeyCode == Keys.D)
                {
                    if (goright == true)
                    {
                        goright = false;
                    }
                }

                //if (e.KeyCode == Keys.Space && ballfrozen == true)
                //{
                //    Ball.Left += ballX;
                //    Ball.Top += ballY;
                //    ballfrozen = false;
                //}
            }
        }

        private void Ball_CheckedChanged(object sender, EventArgs e)
        {

        }

        public void timer1_Tick(object sender, EventArgs e)
        {
            if (goleft && Player.Left >= 1)
            {
                Player.Left -= Speed;
                if (Ball.Bounds.IntersectsWith(Player.Bounds))
                {
                    //Ball.Left += ballX - 90;
                    Ball.Top += ballY - 20;
                }
            }
            if (goright && Player.Left + Player.Width < 500)
            {
                Player.Left += Speed;
                if (Ball.Bounds.IntersectsWith(Player.Bounds))
                {
                    Ball.Left += ballX + 20;
                    
                }
            }

            Ball.Left += ballX;
            Ball.Top += ballY;
            

            if (Ball.Left + Ball.Width > ClientSize.Width || Ball.Left < 0)
            {
                ballX = -ballX;
            }

            if (Ball.Top < 0 || Ball.Bounds.IntersectsWith(Player.Bounds) || Ball.Bounds.IntersectsWith(Enemy.Bounds))
            {
                ballY = -ballY;
                if (Ball.Bounds.IntersectsWith(Player.Bounds))
                {
                    //Console.Beep();                    
                    cpuplayerturn = true;
                    Random rnd = new Random();
                    temprnd = rnd.Next(1, 55);

                    if (Form2.frmObj2.easyselected)
                    {
                        if (temprnd <= 20)
                        {
                            correcthitCPU = true;
                        }
                        else
                        {
                            correcthitCPU = false;
                        }
                    }
                    else if (Form2.frmObj2.mediumselected)
                    {
                        if (temprnd <= 30)
                        {
                            correcthitCPU = true;
                        }
                        else
                        {
                            correcthitCPU = false;
                        }
                    }
                    else if (Form2.frmObj2.hardselected)
                    {
                        if (temprnd <= 51)
                        {
                            correcthitCPU = true;
                        }
                        else
                        {
                            correcthitCPU = false;
                        }
                    }
                    
                }
                else
                {
                    cpuplayerturn = false;
                }
            }

            if (Ball.Bounds.IntersectsWith(GoalPostEnemy.Bounds))
            {
                PlayerScore++;                
                cpuplayerturn = true;                
                correcthitCPU = true;
                Ball.Location = new Point(236, 365);
                timer1.Stop();
                bigscoredisplay();
                if (timer1.Enabled)
                {
                    timer1.Enabled = false;
                }
            }
            else if (Ball.Bounds.IntersectsWith(GoalPostPlayer.Bounds))
            {
                EnemyScore++;                
                Ball.Location = new Point(236, 240);
                correcthitCPU = true;
                timer1.Stop();
                bigscoredisplay();
                if (timer1.Enabled)
                {
                    timer1.Enabled = false;
                }


            }
            label2.Text = EnemyScore.ToString();
            label3.Text = PlayerScore.ToString();

        }

        private void Enemy_Click(object sender, EventArgs e)
        {

        }

        private void bigscoredisplay()
        {
            label1.Text = "Score = Player " + PlayerScore.ToString() + " CPU " + EnemyScore.ToString();
            label1.Visible = true;
            System.Threading.Thread.Sleep(2000);
            
            if (PlayerScore == targetscore)
            {
                System.Windows.Forms.MessageBox.Show("You Win...!!!");
            }
            else if (EnemyScore == targetscore)
            {
                System.Windows.Forms.MessageBox.Show("CPU Win...!!!");
            }
            else
            {
                timer1.Start();
            }
            

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            
            if (cpuplayerturn)
            {
                
                if (correcthitCPU)
                {
                    this.Enemy.Location = new Point(this.Ball.Location.X, this.Enemy.Location.Y);
                }
                else
                {
                    this.Enemy.Location = new Point(this.Ball.Location.X + 20, this.Enemy.Location.Y);
                }
                
            }


            //Enemy Bar to be moved

            //if (Ball.Location.X < 236 && Enemy.Left >= 1)
            //{
            //    if (Enemy.Location.X != Ball.Location.X)
            //    {
            //        Enemy.Left -= EnemySpeed;
            //    }
            //}
            //else if (Ball.Location.X >= 236 && Enemy.Left + Enemy.Width < 500)
            //{
            //    if (Enemy.Location.X != Ball.Location.X)
            //    {
            //        Enemy.Left += EnemySpeed;
            //    }
            //}


        }

        private void GoalPostPlayer_Paint(object sender, PaintEventArgs e)
        {

        }

        private void GoalPostEnemy_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e) //Score Big Text
        {

        }

        private void label2_Click(object sender, EventArgs e) //Enemy Side Score
        {

        }

        private void label3_Click(object sender, EventArgs e) //Player Side Score
        {

        }

        private void gameover()
        {   
            timer1.Stop();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            frmObj = this;
            
        }
    }
}
