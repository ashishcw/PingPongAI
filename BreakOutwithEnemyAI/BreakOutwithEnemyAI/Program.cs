﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace BreakOutwithEnemyAI
{

    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            //Form1 mainform = new Form1();
            //Form2 titleform = new Form2();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new Form1());
            Application.Run(new Form2());
        }
    }
}
