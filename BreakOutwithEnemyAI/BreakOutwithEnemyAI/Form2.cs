﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BreakOutwithEnemyAI
{
    public partial class Form2 : Form
    {

        public bool easyselected, mediumselected, hardselected;
        System.Decimal tempscore;

        //public Form1 mainform = new Form1();
        //public Form2 titleform = new Form2();
        static Form2 _frmObj2;
        public static Form2 frmObj2
        {
            get { return _frmObj2; }
            set { _frmObj2 = value; }
        }

        //public int targetscore;
        public System.Decimal temptargetscore;

        public Form2()
        {
            InitializeComponent();

        }

        public void radioButton1_CheckedChanged(object sender, EventArgs e) //Easy Radio Button
        {

        }

        public void radioButton2_CheckedChanged(object sender, EventArgs e) //Medium Radio Button
        {

        }

        public void radioButton3_CheckedChanged(object sender, EventArgs e) //Hard Radio Button
        {

        }

        public void Start_btn_Click(object sender, EventArgs e) //Start Button
        {
            if (radioButton1.Checked || radioButton2.Checked || radioButton3.Checked)
            {                
                temptargetscore =  numericUpDown2.Value;                
                Form tempnewapplication = new Form1();
                tempnewapplication.Show();                
            }
        }

        private void Exit_btn_Click(object sender, EventArgs e) //Exit Button
        {
            Application.Exit();
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            //numericUpDown2.Value = temptargetscore;
            //tempscore = numericUpDown2.Value;
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            _frmObj2 = this;
        }

        private void TitleFormTimer_Tick(object sender, EventArgs e)
        {
            if (TitleFormTimer.Enabled)
            {
                //numericUpDown1.Value = targetscore;                

                if (radioButton1.Checked)
                {
                    easyselected = true;
                    mediumselected = false;
                    hardselected = false;
                }
                else if (radioButton2.Checked)
                {
                    mediumselected = true;
                    easyselected = false;
                    hardselected = false;
                }
                else if (radioButton3.Checked)
                {
                    hardselected = true;
                    mediumselected = false;
                    easyselected = false;
                }
            }
        }
    }
}
